# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/03/08 23:35:09 by lyhamrou          #+#    #+#              #
#    Updated: 2019/08/10 20:53:15 by lyhamrou         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fdf
MAP_TEST =  maps/francia.fdf
CFLAGS = -Wall -Wextra -Werror
CC = gcc $(CFLAGS)

########################################################### Include Libft & mlx
HEADER = includes/fdf.h
LIBFT_PATH = libft/
INCLUDES = -I includes/ -I libft/ -I minilibx_macos/
LDLIBS = -L./libft/ -lft
MLXLDLIBS = -L./minilibx_macos -lmlx -framework OpenGL -framework AppKit
LIBFT_A = libft/libft.a
###############################################################################

########################################################################## SRCS
SRCS_NAME = main.c parsing.c drawing.c bresenham.c events.c events2.c \
			rotation_x.c rotation_y.c rotation_z.c projection.c \
			left_octant_bresenham.c right_octant_bresenham.c tools.c \
			parsing2.c pannel.c
SRCS_PATH = srcs/
SRC = $(addprefix $(SRCS_PATH),$(SRCS_NAME))
###############################################################################

########################################################################## OBJS
OBJ_NAME = $(SRCS_NAME:.c=.o)
OBJ_PATH = .obj/
OBJ = $(addprefix $(OBJ_PATH),$(OBJ_NAME))
###############################################################################

all: $(NAME)

$(NAME): $(LIBFT_A) $(OBJ)
	@$(CC) $(CFLAGS) -o $(NAME) $(OBJ) $(INCLUDES) $(LDLIBS) $(MLXLDLIBS)

$(LIBFT_A):
	@make -C $(LIBFT_PATH)

$(OBJ_PATH)%.o: $(SRCS_PATH)%.c $(HEADER)
	@mkdir -p $(OBJ_PATH)
	@$(CC) $(CFLAGS) $() $(INCLUDES) -o $@ -c $<

savetheworld: fclean
	@git add .
	@git commit -m "auto-save"
	@git push

norm: fclean
	@norminette $(LIBFT_PATH)
	@norminette $(SRCS_PATH)
	@norminette Includes/

test: all
	@echo "\033[34m\n\n\n---------   \033[33mFDF\033[34m   ---------\n\n\033[0m"
	@cat $(MAP_TEST)
	@echo "\033[34m\n\n---------------------------\n\n\033[0m"
	@./$(NAME) $(MAP_TEST) && $(RM) $(NAME)
	@echo "\n\n\n"

clean:
	@$(RM) -rf $(OBJ_PATH)
	@make clean -C $(LIBFT_PATH)
	@make clean -C minilibx_macos/

fclean: clean
	@$(RM) $(NAME)
	@make fclean -C $(LIBFT_PATH)

re: fclean all

.PHONY : re fclean clean all $(NAME)
