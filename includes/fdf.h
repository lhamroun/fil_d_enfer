/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/01 05:59:45 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/10 20:59:39 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include <math.h>
# include <stdlib.h>
# include <fcntl.h>
# include "libft.h"
# include "mlx.h"

# define BLACK 0x0
# define WHITE 0xffffff
# define GREEN 0x5AE042
# define YELLOW 0xE8D214
# define CYAN 0xffff0
# define BLUE 0x416EF9
# define PURPLE 0x7148E8
# define ORANGE 0xF9903D
# define BROWN 0xD9A827
# define RED 0xB22222
# define MAGENTA 0xD330E9
# define GREY 0x9F9B9F
# define PINK 0xF112C2
# define BLEU "\033[34m"
# define VERT "\033[32m"
# define JAUNE "\033[33m"
# define ROUGE "\033[31m"
# define BLANC "\033[0m"

# define ROTX "Rotation on X : z / x"
# define ROTY "Rotation on Y : n / j"
# define ROTZ "Rotation on Z : k / l"
# define INV "Inversion altitude : i"
# define ALTI "Change altitude : t / g"
# define ZOOOM "Zoom : q / w"
# define RESET "Reset : r"
# define MOV "Move : Arrows"
# define ECHAP "Exit : Esc"

# define KEY_ESC 53
# define KEY_LEFT 123
# define KEY_RIGHT 124
# define KEY_DOWN 125
# define KEY_UP 126
# define KEY_A 12
# define KEY_Z 13
# define KEY_C 8
# define KEY_I 34
# define KEY_W 6
# define KEY_X 7
# define KEY_J 38
# define KEY_N 45
# define KEY_T 17
# define KEY_G 5
# define KEY_L 37
# define KEY_M 41
# define KEY_R 15

# define ZOOM_MIN -5
# define ZOOM_MAX 20
# define MOVE 50
# define ZOOM 1.2
# define ALPHA 0.08
# define ALTITUDE 1.2
# define ALT_MAX 6
# define ALT_MIN -10

# define TITLE "FICELLE DE FERAILLE"
# define WIN_X 1100
# define WIN_Y 800
# define IMG_X 800
# define IMG_Y 800

typedef struct		s_coordonnees
{
	int				color;
	float			x;
	float			y;
	float			z;
	float			xx;
	float			yy;
}					t_cor;

typedef struct		s_env
{
	int				x1;
	int				y1;
	int				x2;
	int				y2;
	int				sx;
	int				sy;
	int				bpp;
	int				sl;
	int				edn;
	int				i;
	int				h_min;
	int				h_max;
	int				alt;
	int				zoom;
	int				*data;
	void			*img;
	void			*mlx_ptr;
	void			*win_ptr;
	float			a23;
	float			a2;
	float			a6;
	t_cor			*cor;
	t_cor			*copy;
}					t_env;

int					pars_map(int fd, t_env *env, int i);
int					init_copy(t_env *env, int i);
void				pull_copy(t_env *env, int i);
void				stock_color(char *tmp, int *i, int j, t_env *env);
int					count_number(char *tmp, int i);
int					check_if_color_is_valid(char *tmp, int i);
int					check_illegal_char(char *tmp, int i);
void				bresenham(t_env *e);
void				key_center(t_env *env, int i);
void				put_pannel(t_env *env);
void				fix_color(t_env *env, int i);
int					key_event(int key, t_env *env);
void				put_map_on_screen(t_env *env, int i);
void				projection_isometric(t_env *env, int i);
void				parallele(t_env *env, int i);
void				key_rotate_x(t_env *env, int key);
void				drawing(t_env env);
void				key_rotate_y(t_env *env, int key);
void				key_rotate_z(t_env *env, int key);
void				key_inverse(t_env *env, int i);
void				key_altitude(t_env *env, int i, int key);
void				one_octant(int er, int dx, int dy, t_env *env);
void				two_octant(int er, int dx, int dy, t_env *env);
void				three_octant(int er, int dx, int dy, t_env *env);
void				four_octant(int er, int dx, int dy, t_env *env);
void				five_octant(int er, int dx, int dy, t_env *env);
void				six_octant(int er, int dx, int dy, t_env *env);
void				seven_octant(int er, int dx, int dy, t_env *env);
void				eight_octant(int er, int dx, int dy, t_env *env);
int					find_altitude_max(t_env *env, int i);
int					find_altitude_min(t_env *env, int i);
int					gradient(t_env *env, int i);
void				reset_param(t_env *env);
int					key_rot(int key, t_env *env);

#endif
