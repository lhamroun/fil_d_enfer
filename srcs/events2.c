/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events2.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/10 17:28:53 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/10 17:30:31 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	reset_param(t_env *env)
{
	pull_copy(env, 0);
	env->alt = 0;
	env->zoom = 0;
}

void	replace_to_center(t_env *env, int p1, int p3)
{
	int		i;

	while (p1 < IMG_X / 2 || p1 > IMG_X / 2)
	{
		i = 0;
		if (p1 < IMG_X / 2)
			while (i < env->sx * env->sy)
				env->cor[i++].xx += 1;
		else if (p1 > IMG_X / 2)
			while (i < env->sx * env->sy)
				env->cor[i++].xx -= 1;
		p1 = p1 < IMG_X / 2 ? p1 + 1 : p1 - 1;
	}
	while (p3 < IMG_Y / 2 || p3 > IMG_Y / 2)
	{
		i = 0;
		if (p3 < IMG_Y / 2)
			while (i < env->sx * env->sy)
				env->cor[i++].yy += 1;
		else if (p3 > IMG_Y / 2)
			while (i < env->sx * env->sy)
				env->cor[i++].yy -= 1;
		p3 = p3 < IMG_Y / 2 ? p3 + 1 : p3 - 1;
	}
}

void	key_center(t_env *env, int i)
{
	int		p1;
	int		p2;
	int		p3;
	int		p4;

	p1 = INT_MAX;
	p2 = INT_MIN;
	p3 = INT_MAX;
	p4 = INT_MIN;
	while (i < env->sx * env->sy)
	{
		if (env->cor[i].xx < p1)
			p1 = env->cor[i].xx;
		if (env->cor[i].xx > p2)
			p2 = env->cor[i].xx;
		if (env->cor[i].yy < p3)
			p3 = env->cor[i].yy;
		if (env->cor[i].yy > p4)
			p4 = env->cor[i].yy;
		++i;
	}
	replace_to_center(env, (p1 + p2) / 2, (p3 + p4) / 2);
}

void	key_inverse(t_env *env, int i)
{
	while (i < env->sx * env->sy)
	{
		env->cor[i].z *= -1;
		++i;
	}
	projection_isometric(env, 0);
	key_center(env, 0);
	env->zoom = 0;
}

void	key_altitude(t_env *env, int i, int key)
{
	env->zoom = 0;
	if (key == KEY_T && env->alt < ALT_MAX)
	{
		while (i < env->sx * env->sy)
		{
			env->cor[i].z *= ALTITUDE;
			++i;
		}
		env->alt++;
	}
	else if (key == KEY_G && env->alt > ALT_MIN)
	{
		while (i < env->sx * env->sy)
		{
			env->cor[i].z /= ALTITUDE;
			++i;
		}
		env->alt--;
	}
	projection_isometric(env, 0);
	key_center(env, 0);
}
