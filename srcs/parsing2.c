/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/10 17:34:35 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/10 17:48:16 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		stock_color(char *tmp, int *i, int j, t_env *env)
{
	*i = *i + 3;
	env->cor[j].color = ft_atoi_base(&tmp[*i], 16);
	while (tmp[*i] && tmp[*i] != ' ' && tmp[*i] != '\n')
		*i = *i + 1;
}

int			count_number(char *tmp, int i)
{
	int		n;

	n = 0;
	while (tmp[i])
	{
		if ((tmp[i] == '-' || tmp[i] == '+')
				&& (tmp[i + 1] < '0' || tmp[i + 1] > '9'))
			return (0);
		if (tmp[i] == '-' || tmp[i] == '+' || ft_isdigit(tmp[i]) == 1)
		{
			while (tmp[i] == '-' || tmp[i] == '+' || ft_isdigit(tmp[i]) == 1)
				++i;
			n++;
		}
		if (tmp[i] == ',')
			while (tmp[i + 1] != ' ' && tmp[i + 1])
				++i;
		++i;
	}
	return (n);
}

int			check_illegal_char(char *tmp, int i)
{
	while (tmp[i])
	{
		if (tmp[i] != '-' && tmp[i] != '+' && tmp[i] != ' ' && tmp[i] != ','
			&& tmp[i] != 'x' && tmp[i] != 'a' && tmp[i] != 'b' && tmp[i] != 'c'
			&& tmp[i] != 'd' && tmp[i] != 'e' && tmp[i] != 'f' && tmp[i] != 'A'
			&& tmp[i] != 'B' && tmp[i] != 'C' && tmp[i] != 'D' && tmp[i] != 'E'
			&& tmp[i] != 'F' && tmp[i] != 'F' && tmp[i] != '\n'
			&& (tmp[i] < '0' || tmp[i] > '9'))
			return (0);
		++i;
	}
	return (1);
}

int			check_if_color_is_valid(char *tmp, int i)
{
	int		j;

	while (tmp[i])
	{
		j = 0;
		if (tmp[i] == ',')
		{
			if (tmp[++i] != '0')
				return (0);
			if (tmp[++i] != 'x')
				return (0);
			++i;
			while (tmp[i + j] != ' ' && tmp[i + j] != '\n')
				++j;
			if (j != 3 && j != 6)
				return (0);
			else
				i += j;
		}
		++i;
	}
	return (1);
}
