/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotation_z.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/10 17:30:48 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/10 17:30:49 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	key_rotate_z_m(t_env *env, int i)
{
	while (i < env->sx * env->sy)
	{
		env->cor[i].x = env->cor[i].x * cos(-ALPHA)
			+ env->cor[i].z * sin(-ALPHA);
		env->cor[i].z = env->cor[i].z * cos(-ALPHA)
			- env->cor[i].x * sin(-ALPHA);
		++i;
	}
}

void	key_rotate_z_l(t_env *env, int i)
{
	while (i < env->sx * env->sy)
	{
		env->cor[i].x = env->cor[i].x * cos(ALPHA)
			+ env->cor[i].z * sin(ALPHA);
		env->cor[i].z = env->cor[i].z * cos(ALPHA)
			- env->cor[i].x * sin(ALPHA);
		++i;
	}
}

void	key_rotate_z(t_env *env, int key)
{
	if (key == KEY_L)
		key_rotate_z_l(env, 0);
	else if (key == KEY_M)
		key_rotate_z_m(env, 0);
	projection_isometric(env, 0);
	key_center(env, 0);
}
