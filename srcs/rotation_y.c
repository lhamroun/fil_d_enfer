/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotation_y.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/10 17:30:42 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/10 17:30:44 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	key_rotate_y_w(t_env *env, int i)
{
	while (i < env->sx * env->sy)
	{
		env->cor[i].y = env->cor[i].y * cos(ALPHA)
			- env->cor[i].z * sin(ALPHA);
		env->cor[i].z = env->cor[i].y * sin(ALPHA)
			+ env->cor[i].z * cos(ALPHA);
		++i;
	}
}

void	key_rotate_y_x(t_env *env, int i)
{
	while (i < env->sx * env->sy)
	{
		env->cor[i].y = env->cor[i].y * cos(-ALPHA)
			- env->cor[i].z * sin(-ALPHA);
		env->cor[i].z = env->cor[i].y * sin(-ALPHA)
			+ env->cor[i].z * cos(-ALPHA);
		++i;
	}
}

void	key_rotate_y(t_env *env, int key)
{
	if (key == KEY_W)
		key_rotate_y_w(env, 0);
	else if (key == KEY_X)
		key_rotate_y_x(env, 0);
	projection_isometric(env, 0);
	key_center(env, 0);
}
