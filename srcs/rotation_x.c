/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotation_x.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/10 17:30:38 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/10 17:30:39 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	key_rotate_x_n(t_env *env, int i)
{
	while (i < env->sx * env->sy)
	{
		env->cor[i].x = env->cor[i].x * cos(ALPHA)
			- env->cor[i].y * sin(ALPHA);
		env->cor[i].y = env->cor[i].x * sin(ALPHA)
			+ env->cor[i].y * cos(ALPHA);
		++i;
	}
}

void	key_rotate_x_j(t_env *env, int i)
{
	while (i < env->sx * env->sy)
	{
		env->cor[i].x = env->cor[i].x * cos(-ALPHA)
			- env->cor[i].y * sin(-ALPHA);
		env->cor[i].y = env->cor[i].x * sin(-ALPHA)
			+ env->cor[i].y * cos(-ALPHA);
		++i;
	}
}

void	key_rotate_x(t_env *env, int key)
{
	if (key == KEY_J)
		key_rotate_x_j(env, 0);
	else if (key == KEY_N)
		key_rotate_x_n(env, 0);
	projection_isometric(env, 0);
	key_center(env, 0);
}
