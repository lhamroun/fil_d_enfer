/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/19 16:33:31 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/10 19:09:30 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	init_map(t_env *e)
{
	e->copy = NULL;
	e->cor = NULL;
	e->a23 = sqrt(2.0 / 3);
	e->a2 = sqrt(2.0);
	e->a6 = sqrt(6.0);
	e->h_min = 0;
	e->h_max = 0;
	e->alt = 0;
	e->zoom = 0;
	e->x1 = 0;
	e->x2 = 0;
	e->y1 = 0;
	e->y2 = 0;
	e->sx = 0;
	e->sy = 0;
	e->i = 0;
	e->mlx_ptr = NULL;
	e->win_ptr = NULL;
	e->img = NULL;
	e->data = NULL;
}

static int	free_les_bails(t_env *env)
{
	if (env)
	{
		if (env->cor)
			free(env->cor);
		if (env->copy)
			free(env->copy);
		if (env->mlx_ptr && env->win_ptr)
			mlx_destroy_window(env->mlx_ptr, env->win_ptr);
	}
	return (0);
}

static void	init_mlx(t_env *e)
{
	e->mlx_ptr = mlx_init();
	e->win_ptr = mlx_new_window(e->mlx_ptr, WIN_X, WIN_Y, TITLE);
	e->img = mlx_new_image(e->mlx_ptr, IMG_X, IMG_Y);
	e->data = (int *)mlx_get_data_addr(e->img, &e->bpp, &e->sl, &e->edn);
	ft_memset(e->data, 0, sizeof(int) * e->sx * e->sy);
	e->h_min = find_altitude_min(e, 0);
	e->h_max = find_altitude_max(e, 0);
}

int			main(int ac, char **av)
{
	int		fd;
	t_env	env;

	if (ac != 2 || (fd = open(av[1], O_RDONLY)) == -1)
		return (0);
	init_map(&env);
	if (pars_map(fd, &env, 0) == 0)
		return (free_les_bails(&env));
	init_mlx(&env);
	drawing(env);
	close(fd);
	return (free_les_bails(&env));
}
