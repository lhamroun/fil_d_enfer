/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/09 21:20:09 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/27 17:58:45 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static int	free_pars_map(char **tmp, char **buf)
{
	ft_putendl_fd("Error map format", 2);
	if (tmp)
		ft_strdel(tmp);
	if (buf)
		ft_strdel(buf);
	return (0);
}

static void	bouche_trou(char *tmp, int *i, int *y)
{
	if (tmp[*i] == '\n')
	{
		++*y;
		++*i;
	}
	else
		++*i;
}

int			str_to_struct(char *tmp, int i, t_env *env, int y)
{
	int		j;

	j = 0;
	if (!(env->cor = (t_cor *)malloc(sizeof(t_cor) * env->sx * env->sy)))
		return (0);
	while (j < env->sx * env->sy && tmp[i])
	{
		if (tmp[i] == '-' || tmp[i] == '+' || ft_isdigit(tmp[i]) == 1)
		{
			env->cor[j].x = (float)(j % env->sx);
			env->cor[j].y = (float)y;
			env->cor[j].z = (float)ft_atoi(&tmp[i]);
			i = i + (int)number_of_digit(ft_atoi(&tmp[i]));
			if (tmp[i] == ',')
				stock_color(tmp, &i, j, env);
			else
				env->cor[j].color = 0;
			++j;
		}
		else
			bouche_trou(tmp, &i, &y);
	}
	return (1);
}

int			pars_map(int fd, t_env *env, int i)
{
	char	*buf;
	char	*tmp;
	int		n;

	tmp = ft_strnew(0);
	while (get_next_line(fd, &buf) > 0)
	{
		if (check_illegal_char(buf, 0) == 0)
			return (free_pars_map(&buf, &tmp));
		i == 0 ? env->sx = count_number(buf, 0) : 1;
		i != 0 ? n = count_number(buf, 0) : 1;
		if (i != 0 && n != env->sx)
			return (free_pars_map(&buf, &tmp));
		if (check_if_color_is_valid(buf, 0) == 0)
			return (free_pars_map(&buf, &tmp));
		if ((tmp = ft_strjoinfree(tmp, buf, 3)) == NULL)
			return (free_pars_map(&buf, &tmp));
		++i;
	}
	env->sy = i;
	if (str_to_struct(tmp, 0, env, 0) == 0)
		return (free_pars_map(&buf, &tmp));
	if (tmp)
		ft_strdel(&tmp);
	return (1);
}
