/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   left_octant_bresenham.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/10 17:31:32 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/10 17:31:34 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	eight_octant(int er, int dx, int dy, t_env *e)
{
	er = dy;
	dy = er * 2;
	dx = dx * 2;
	while (1)
	{
		if (e->x1 > 0 && e->y1 > 0 && e->x1 < IMG_X && e->y1 < IMG_Y)
		{
			if ((e->y1 * e->sl / 4) + e->x1 < IMG_Y * IMG_X
				&& (e->y1 * e->sl / 4) + e->x1 > 0)
				e->data[e->x1 + e->y1 * (e->sl / 4)] = e->cor[e->i].color;
		}
		if ((e->y1 = e->y1 - 1) == e->y2)
			break ;
		if ((er = er - dx) >= 0)
		{
			e->x1 = e->x1 - 1;
			er = er + dy;
		}
	}
}

void	seven_octant(int er, int dx, int dy, t_env *e)
{
	er = dx;
	dx = er * 2;
	dy = dy * 2;
	while (1)
	{
		if (e->x1 > 0 && e->y1 > 0 && e->x1 < IMG_X && e->y1 < IMG_Y)
		{
			if ((e->y1 * e->sl / 4) + e->x1 < IMG_Y * IMG_X
				&& (e->y1 * e->sl / 4) + e->x1 > 0)
				e->data[e->x1 + e->y1 * (e->sl / 4)] = e->cor[e->i].color;
		}
		if ((e->x1 = e->x1 - 1) == e->x2)
			break ;
		if ((er = er - dy) >= 0)
		{
			e->y1 = e->y1 - 1;
			er = er + dx;
		}
	}
}

void	six_octant(int er, int dx, int dy, t_env *e)
{
	er = dx;
	dx = er * 2;
	dy = dy * 2;
	while (1)
	{
		if (e->x1 > 0 && e->y1 > 0 && e->x1 < IMG_X && e->y1 < IMG_Y)
		{
			if ((e->y1 * e->sl / 4) + e->x1 < IMG_Y * IMG_X
				&& (e->y1 * e->sl / 4) + e->x1 > 0)
				e->data[e->x1 + e->y1 * (e->sl / 4)] = e->cor[e->i].color;
		}
		if ((e->x1 = e->x1 - 1) == e->x2)
			break ;
		if ((er = er + dy) >= 0)
		{
			e->y1 = e->y1 + 1;
			er = er + dx;
		}
	}
}

void	five_octant(int er, int dx, int dy, t_env *e)
{
	er = dy;
	dy = er * 2;
	dx = dx * 2;
	while (1)
	{
		if (e->x1 > 0 && e->y1 > 0 && e->x1 < IMG_X && e->y1 < IMG_Y)
		{
			if ((e->y1 * e->sl / 4) + e->x1 < IMG_Y * IMG_X
				&& (e->y1 * e->sl / 4) + e->x1 > 0)
				e->data[(e->y1 * e->sl / 4) + e->x1] = e->cor[e->i].color;
		}
		if ((e->y1 = e->y1 + 1) == e->y2)
			break ;
		if ((er = er + dx) < 0)
		{
			e->x1 = e->x1 - 1;
			er = er + dy;
		}
	}
}
