/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   projection.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/10 17:31:52 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/10 17:31:55 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	parallele(t_env *env, int i)
{
	int		pas;
	int		dx;
	int		dy;

	pas = IMG_X / env->sx < IMG_Y / env->sy ? IMG_X / env->sx : IMG_Y / env->sy;
	dx = env->cor[i].x;
	dy = env->cor[i].y;
	while (i < env->sx * env->sy)
	{
		env->cor[i].x = env->cor[i].x * pas;
		env->cor[i].y = env->cor[i].y * pas;
		env->cor[i].z = env->cor[i].z * pas;
		++i;
	}
}

void	pull_copy(t_env *env, int i)
{
	while (i < env->sx * env->sy)
	{
		env->cor[i].color = env->copy[i].color;
		env->cor[i].x = env->copy[i].x;
		env->cor[i].y = env->copy[i].y;
		env->cor[i].z = env->copy[i].z;
		env->cor[i].xx = env->copy[i].xx;
		env->cor[i].yy = env->copy[i].yy;
		++i;
	}
}

int		init_copy(t_env *env, int i)
{
	if (env->copy == NULL)
	{
		if (!(env->copy = (t_cor *)ft_memalloc(env->sx * env->sy
			* sizeof(t_cor))))
			return (0);
	}
	while (i < env->sx * env->sy)
	{
		env->copy[i].color = env->cor[i].color;
		env->copy[i].x = env->cor[i].x;
		env->copy[i].y = env->cor[i].y;
		env->copy[i].z = env->cor[i].z;
		env->copy[i].xx = env->cor[i].xx;
		env->copy[i].yy = env->cor[i].yy;
		++i;
	}
	return (1);
}

void	projection_isometric(t_env *env, int i)
{
	while (i < env->sx * env->sy)
	{
		env->cor[i].xx = (env->a2 / 2) * (env->cor[i].x - env->cor[i].y);
		env->cor[i].yy = -(env->a23 * env->cor[i].z)
			+ ((1 / env->a6) * (env->cor[i].x + env->cor[i].y));
		++i;
	}
}
