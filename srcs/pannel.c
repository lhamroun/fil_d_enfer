/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   pannel.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/10 19:10:28 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/10 20:54:28 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	put_bordure(t_env *env, int x, int i)
{
	while (i < x)
	{
		mlx_pixel_put(env->mlx_ptr, env->win_ptr, IMG_X + i, 0, WHITE);
		mlx_pixel_put(env->mlx_ptr, env->win_ptr, IMG_X + i, 1, WHITE);
		mlx_pixel_put(env->mlx_ptr, env->win_ptr, IMG_X + i, 2, WHITE);
		mlx_pixel_put(env->mlx_ptr, env->win_ptr, IMG_X + i, WIN_Y - 1, WHITE);
		mlx_pixel_put(env->mlx_ptr, env->win_ptr, IMG_X + i, WIN_Y - 2, WHITE);
		mlx_pixel_put(env->mlx_ptr, env->win_ptr, IMG_X + i, WIN_Y - 3, WHITE);
		++i;
	}
	i = 0;
	while (i < WIN_Y)
	{
		mlx_pixel_put(env->mlx_ptr, env->win_ptr, IMG_X, i, WHITE);
		mlx_pixel_put(env->mlx_ptr, env->win_ptr, IMG_X + 1, i, WHITE);
		mlx_pixel_put(env->mlx_ptr, env->win_ptr, IMG_X + 2, i, WHITE);
		mlx_pixel_put(env->mlx_ptr, env->win_ptr, WIN_X - 1, i, WHITE);
		mlx_pixel_put(env->mlx_ptr, env->win_ptr, WIN_X - 2, i, WHITE);
		mlx_pixel_put(env->mlx_ptr, env->win_ptr, WIN_X - 3, i, WHITE);
		++i;
	}
}

void	put_infos(t_env *env)
{
	mlx_string_put(env->mlx_ptr, env->win_ptr, IMG_X + 30, 150, WHITE, ROTX);
	mlx_string_put(env->mlx_ptr, env->win_ptr, IMG_X + 30, 200, WHITE, ROTY);
	mlx_string_put(env->mlx_ptr, env->win_ptr, IMG_X + 30, 250, WHITE, ROTZ);
	mlx_string_put(env->mlx_ptr, env->win_ptr, IMG_X + 30, 300, WHITE, INV);
	mlx_string_put(env->mlx_ptr, env->win_ptr, IMG_X + 30, 350, WHITE, ALTI);
	mlx_string_put(env->mlx_ptr, env->win_ptr, IMG_X + 30, 400, WHITE, RESET);
	mlx_string_put(env->mlx_ptr, env->win_ptr, IMG_X + 30, 450, WHITE, ZOOOM);
	mlx_string_put(env->mlx_ptr, env->win_ptr, IMG_X + 30, 500, WHITE, MOV);
	mlx_string_put(env->mlx_ptr, env->win_ptr, IMG_X + 30, 550, WHITE, ECHAP);
}

void	put_pannel(t_env *env)
{
	int		x;

	x = WIN_X - IMG_X;
	put_bordure(env, x, 0);
	put_infos(env);
}
