/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/10 17:31:08 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/10 21:51:10 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	affect_zoom(t_env *env, int t, int i)
{
	if (t == 0 && env->zoom < ZOOM_MAX)
	{
		while (i < env->sx * env->sy)
		{
			env->cor[i].xx *= ZOOM;
			env->cor[i].yy *= ZOOM;
			++i;
		}
		env->zoom++;
	}
	else if (t == 1 && env->zoom > ZOOM_MIN)
	{
		while (i < env->sx * env->sy)
		{
			env->cor[i].xx /= ZOOM;
			env->cor[i].yy /= ZOOM;
			++i;
		}
		env->zoom--;
	}
}

void	key_zoom(int key, t_env *env)
{
	if (key == KEY_A && env->cor[1].xx - env->cor[0].xx < WIN_X / 2)
		affect_zoom(env, 0, 0);
	else if (key == KEY_Z)
		affect_zoom(env, 1, 0);
	key_center(env, 0);
}

void	key_move(int key, t_env *env)
{
	int		i;

	i = 0;
	if (key == KEY_RIGHT)
	{
		while (i < env->sx * env->sy)
			env->cor[i++].xx += MOVE;
	}
	else if (key == KEY_LEFT)
	{
		while (i < env->sx * env->sy)
			env->cor[i++].xx -= MOVE;
	}
	else if (key == KEY_DOWN)
	{
		while (i < env->sx * env->sy)
			env->cor[i++].yy += MOVE;
	}
	else if (key == KEY_UP)
	{
		while (i < env->sx * env->sy)
			env->cor[i++].yy -= MOVE;
	}
}

int		key_rot(int key, t_env *env)
{
	env->zoom = 0;
	if (key == KEY_W || key == KEY_X)
	{
		key_rotate_y(env, key);
		return (1);
	}
	else if (key == KEY_J || key == KEY_N)
	{
		key_rotate_x(env, key);
		return (1);
	}
	else if (key == KEY_L || key == KEY_M)
	{
		key_rotate_z(env, key);
		return (1);
	}
	return (0);
}

int		key_event(int key, t_env *env)
{
	int		i;

	i = 0;
	if (key == KEY_LEFT || key == KEY_RIGHT || key == KEY_DOWN || key == KEY_UP)
		key_move(key, env);
	else if (key == KEY_A || key == KEY_Z)
		key_zoom(key, env);
	else if (key == KEY_C)
		key_center(env, 0);
	else if (key == KEY_T || key == KEY_G)
		key_altitude(env, 0, key);
	else if (key == KEY_W || key == KEY_X || key == KEY_N || key == KEY_J
		|| key == KEY_L || key == KEY_M)
		i = key_rot(key, env);
	else if (key == KEY_I)
		key_inverse(env, 0);
	else if (key == KEY_R)
		reset_param(env);
	else if (key == KEY_ESC)
		exit(0);
	put_map_on_screen(env, 0);
	return (1);
}
