/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bresenham.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/10 17:31:26 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/10 17:31:27 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	bresenham_horizontal(t_env *e, int t)
{
	if (t == 0)
	{
		while (e->x1 <= e->x2)
		{
			if (e->x1 < IMG_X && e->x1 > 0 && e->y1 > 0 && e->y1 < IMG_Y
				&& (e->y1 * e->sl / 4) + e->x1 < IMG_Y * IMG_X
				&& (e->y1 * e->sl / 4) + e->x1 > 0)
				e->data[(e->y1 * e->sl / 4) + e->x1] = e->cor[e->i].color;
			e->x1++;
		}
	}
	else
	{
		while (e->x1 > e->x2)
		{
			if (e->x1 < IMG_X && e->x1 > 0 && e->y1 > 0 && e->y1 < IMG_Y
				&& (e->y1 * e->sl / 4) + e->x1 < IMG_Y * IMG_X
				&& (e->y1 * e->sl / 4) + e->x1 > 0)
				e->data[(e->y1 * e->sl / 4) + e->x1] = e->cor[e->i].color;
			e->x1--;
		}
	}
}

void	bresenham_vertical(int dy, t_env *e)
{
	if ((dy = e->y2 - e->y1) != 0)
	{
		if (dy > 0)
		{
			while (e->y1 <= e->y2)
			{
				if (e->y1 > 0 && e->y1 < IMG_Y && e->x1 > 0 && e->x1 < IMG_X
					&& (e->y1 * e->sl / 4) + e->x1 < IMG_Y * IMG_X
					&& (e->y1 * e->sl / 4) + e->x1 > 0)
					e->data[(e->y1 * e->sl / 4) + e->x1] = e->cor[e->i].color;
				e->y1++;
			}
		}
		else if (dy < 0)
		{
			while (e->y1 > e->y2)
			{
				if (e->y1 > 0 && e->y1 < IMG_Y && e->x1 > 0 && e->x1 < IMG_X
					&& (e->y1 * e->sl / 4) + e->x1 < IMG_Y * IMG_X
					&& (e->y1 * e->sl / 4) + e->x1 > 0)
					e->data[(e->y1 * e->sl / 4) + e->x1] = e->cor[e->i].color;
				e->y1--;
			}
		}
	}
}

void	left_octant(int er, int dx, int dy, t_env *e)
{
	if (dy > 0)
	{
		if (-dx >= dy)
			six_octant(er, dx, dy, e);
		else
			five_octant(er, dx, dy, e);
	}
	else if (dy < 0 && dx < 0)
	{
		if (dx <= dy)
			seven_octant(er, dx, dy, e);
		else if (dx > dy)
			eight_octant(er, dx, dy, e);
	}
}

void	right_octant(int er, int dx, int dy, t_env *e)
{
	if (dy > 0)
	{
		if (dx >= dy)
			three_octant(er, dx, dy, e);
		else
			four_octant(er, dx, dy, e);
	}
	else if (dy < 0 && dx > 0)
	{
		if (dx >= -dy)
			two_octant(er, dx, dy, e);
		else
			one_octant(er, dx, dy, e);
	}
}

void	bresenham(t_env *e)
{
	int		dx;
	int		dy;
	int		er;

	er = 0;
	dy = 0;
	if ((dx = e->x2 - e->x1) != 0)
	{
		if (dx > 0)
		{
			if ((dy = e->y2 - e->y1) != 0)
				right_octant(er, dx, dy, e);
			else if (dy == 0)
				bresenham_horizontal(e, 0);
		}
		else if (dx < 0)
		{
			if ((dy = e->y2 - e->y1) != 0)
				left_octant(er, dx, dy, e);
			else if (dy == 0 && dx < 0)
				bresenham_horizontal(e, 1);
		}
	}
	else if (dx == 0)
		bresenham_vertical(dy, e);
}
