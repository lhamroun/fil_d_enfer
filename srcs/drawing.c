/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   drawing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/10 20:56:50 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/10 21:50:27 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	iso_y_to_bres(t_env *env, int i)
{
	if (i + env->sx < env->sx * env->sy)
	{
		env->x1 = (int)env->cor[i].xx;
		env->y1 = (int)env->cor[i].yy;
		env->x2 = (int)env->cor[i + env->sx].xx;
		env->y2 = (int)env->cor[i + env->sx].yy;
	}
	else
	{
		env->x1 = 0;
		env->y1 = 0;
		env->x2 = 0;
		env->y2 = 0;
	}
}

static void	iso_x_to_bres(t_env *env, int i)
{
	if ((i + 1) % env->sx != 0)
	{
		env->x1 = (int)env->cor[i].xx;
		env->y1 = (int)env->cor[i].yy;
		env->x2 = (int)env->cor[i + 1].xx;
		env->y2 = (int)env->cor[i + 1].yy;
	}
	else
	{
		env->x1 = 0;
		env->y1 = 0;
		env->x2 = 0;
		env->y2 = 0;
	}
}

void		put_map_on_screen(t_env *env, int i)
{
	ft_memset(env->data, 0, sizeof(int) * (size_t)IMG_X * (size_t)IMG_Y);
	while (i + 1 < env->sx * env->sy)
	{
		env->i = i;
		iso_x_to_bres(env, i);
		bresenham(env);
		iso_y_to_bres(env, i);
		bresenham(env);
		++i;
	}
	mlx_put_image_to_window(env->mlx_ptr, env->win_ptr, env->img, 0, 0);
}

void		drawing(t_env env)
{
	put_pannel(&env);
	parallele(&env, 0);
	projection_isometric(&env, 0);
	key_center(&env, 0);
	fix_color(&env, 0);
	if (init_copy(&env, 0) == 0)
	{
		ft_putendl_fd("Error malloc env.copy", 2);
		return ;
	}
	put_map_on_screen(&env, 0);
	if (!(mlx_key_hook(env.win_ptr, key_event, &env)))
		return ;
	mlx_loop(env.mlx_ptr);
}
