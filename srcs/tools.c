/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/10 17:32:03 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/08/10 17:32:21 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		find_altitude_min(t_env *env, int i)
{
	int		min;

	min = INT_MAX;
	while (i < env->sx * env->sy)
	{
		if ((int)env->cor[i].z < min)
			min = (int)env->cor[i].z;
		++i;
	}
	return (min);
}

int		find_altitude_max(t_env *env, int i)
{
	int		max;

	max = INT_MIN;
	while (i < env->sx * env->sy)
	{
		if ((int)env->cor[i].z > max)
			max = (int)env->cor[i].z;
		++i;
	}
	return (max);
}

/*
**	This function fill the value of x1, y1 and x2, y2 for bresenham without
**	line behind a volume
*/

void	fix_color(t_env *env, int i)
{
	int		pas;

	pas = IMG_X / env->sx < IMG_Y / env->sy ? IMG_X / env->sx : IMG_Y / env->sy;
	while (i < env->sx * env->sy)
	{
		if (env->cor[i].color == 0)
		{
			if ((int)env->cor[i].z / pas <= env->h_min)
				env->cor[i].color = CYAN;
			else if ((int)env->cor[i].z / pas >= env->h_max)
				env->cor[i].color = BROWN;
			else
				env->cor[i].color = GREEN;
			++i;
		}
		else
			++i;
	}
}
